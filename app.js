const express = require('express');

const app = express();

const authService = require('./src/google-auth-service');

const config = require('./src/config/client_secret.json');

app.set('view engine', 'ejs');

app.get('/', (req, res) => {
  res.render('index');
});

app.get('/signin', (req, res) => {
  const redirectUrl = authService.init({
    clientId: config.web.client_id,
    clientSecret: config.web.client_secret,
    redirectUrl: 'http://localhost:8000/google-auth',
    accessType: 'offline',
    scope: ['https://www.googleapis.com/auth/plus.me'],
    prompt: 'consent',
  });
  res.redirect(redirectUrl);
});

app.get('/google-auth', (req, res) => {
  authService.getAccessToken(
    req.query.code,
    (token) => {
      authService.setCredentials(token);
      res.render('redirect', {
        success: true,
        access_token: token.access_token,
        refresh_token: token.refresh_token || undefined,
      });
    },
    ({ response }) => {
      if (response.data.error) {
        res.render('redirect', {
          success: false,
          error: response.data.error_description,
        });
      }
    },
  );
});

app.get('/dashboard', (req, res) => {
  res.render('dashboard');
});

app.listen(8000);
