To run this project, clone the repo and perform `npm install`, configure the project and run.


##### Configure project.

Create a new file in `src/config` as `client_secret.json` in this format : 

```javascript
{"web":
    {
    "client_id": //YOUR CLIENT_ID,
    "client_secret": //YOUR CLIENT_SECRET,
    }
}
```

To get your client id and client secret follow these steps : 

* Go to the [Google API Console](https://console.developers.google.com/).

* From the project drop-down, select an existing project , or create a new one by selecting **Create a new project**.

* In the sidebar under **APIs & Services**, select **Credentials**, then select the **OAuth consent screen** tab.
    * Choose an Email Address, specify a Product Name, and press Save.
    
* In the **Credentials** tab, select the **Create credentials** drop-down list, and choose **OAuth client ID**.

* Under **__Application type__**, select **Web application**. 
    * In the __Authorized JavaScript origins__ field, enter the origin for your app.
    ( In this case it would be localhost while running on your system, ex. `http://localhost:8000` )
    * In the __Authorized redirect URI__ field enter the redirect url to your domain after login. 
    ( In this case since it is on localhost, it will be `http://localhost:8000/google-auth` )
    * Press the __Create__ button.
    
* From the resulting __OAuth client dialog box__, copy the **Client ID** and **Client secret** and add it to your config file.

##### Run the project

On the root of project, run `npm run server` and visit `http://localhost:8000`
