const { google } = require('googleapis');

let authClient;

function initialiseOAuth({
  clientId,
  clientSecret,
  redirectUrl,
  accessType,
  scope,
  prompt,
}) {
  authClient = new google.auth.OAuth2(clientId, clientSecret, redirectUrl);

  const url = authClient.generateAuthUrl({
    access_type: accessType,
    scope,
    prompt,
  });
  return url;
}

function getAccessToken(code, fetched, error) {
  const payload = authClient.getToken(code);
  payload
    .then(({ tokens }) => {
      fetched(tokens);
    })
    .catch((err) => {
      error(err);
    });
}

function setCredentials(token) {
  try {
    authClient.setCredentials(token);
  } catch (err) {
    console.warn('error saving credentials');
  }
}

module.exports = {
  getAccessToken,
  init: initialiseOAuth,
  setCredentials,
};
